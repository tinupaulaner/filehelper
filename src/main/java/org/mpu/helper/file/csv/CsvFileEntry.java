package org.mpu.helper.file.csv;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Pulfer
 * @version 1.00 (updated 29.03.2019)
 * @since 29.03.2019
 */
public class CsvFileEntry {

    private List<String> vals = new ArrayList<>();
    private String val;

    public List<String> getVals() {
        return vals;
    }

    public void setVals(List<String> vals) {
        this.vals = vals;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
