package org.mpu.helper.file.csv;

import org.mpu.helper.file.FileTools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Read, Write, Append, Delete or Search lines of a File with or without CSV-Values
 * extends a java.io.File
 *
 * @author Martin Pulfer
 * @version 3.01 (updated 29.03.2019)
 * @since 16.10.2009
 * @see File
 *
 */
public class CsvFile extends File {

    private static final long serialVersionUID = 1078474280306433458L;

    public final static String CSV_SEPERATOR = ";";

    private int accessMode;
    public final static int READ_ONLY_MODE = 0;
    public final static int WRITE_MODE = 1;

    private String finalFileName;

    private List<String> readedContent = null;

    /**
     *
     * @param fileName the filepath and -name of the Csv-File eg. "C:/file.csv".
     * @param accessMode use CsvFile.READ_ONLY_MODE or CsvFile.WRITE_MODE
     * @param overwriteExistingLines if true, entries of existing files will we overwritten otherwise added.
     * use false if you want to delete or add unique lines
     * @throws IOException occurs when the file can't be accessed
     */
    public CsvFile(String fileName, int accessMode, boolean overwriteExistingLines) throws IOException  {

        super(accessMode == WRITE_MODE ? File.createTempFile("csv", null).getAbsolutePath() : fileName);
        this.accessMode = accessMode;

        File f = new File(fileName);
        if (f.exists())	readedContent = FileTools.getValuesInFile(f);

        switch (accessMode){

            case READ_ONLY_MODE:
                if (readedContent == null) throw new IOError(new Throwable("file doesn't exist. change accessMode."));
                break;

            case WRITE_MODE:
                if (readedContent == null) readedContent = new ArrayList<String>();
                else if (overwriteExistingLines) readedContent.clear();
                break;

            default:
                throw new IOError(new Throwable("accessMode can only be 0 or 1"));
        }

        finalFileName = fileName;

    }


    /**
     * @return the number of lines the file got
     */
    public int getLines() {
        return readedContent.size();
    }

    public String getFinalFileName(){
        return this.finalFileName;
    }

    /**
     * @param lineNumber the lineNumber to access (first is 0)
     * @return a list of all values from the line, separated through the CSV_SEPERATOR
     */
    public List<String> getCsvValues (int lineNumber){

        try {
            List<String> vals = new ArrayList<String>();
            String [] values = this.readedContent.get(lineNumber).split(CSV_SEPERATOR);

            for (int i=0;i<values.length;i++){
                vals.add(values[i]);
            }

            return vals;
        }

        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param lineNumber the lineNumber to access (first is 0)
     * @return full line
     */
    public String getFullLineValue (int lineNumber){
        try {
            return this.readedContent.get(lineNumber);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Searching for a unique line containing a unique value
     * @param searchSubString the substring to search for
     * @param pos the row position (first = 0)
     * @return a list of all values from the frist or unique found line, separated through the CSV_SEPERATOR or null if not found
     */
    public List<String> getCsvValues (String searchSubString, int pos){

        for (int i = 0; i < getLines();i++){
            List<String> vals = getCsvValues(i);

            if (pos + 1 <= vals.size()){
                if (vals.get(pos).contains(searchSubString)){
                    return vals;
                }
            }
        }
        return null;
    }

    /**
     * @return all lines of the csv file separated in a list of vals through the CSV_SEPERATOR
     */
    public List<List<String>> getAllCsvLines() {
        List<List<String>> lines = new ArrayList<List<String>>();
        for (int i = 0; i < getLines();i++) lines.add(getCsvValues(i));
        return lines;
    }

    /**
     * @return all lines of the csv file
     */
    public List<String> getAllLines() {
        return this.readedContent;
    }

    /**
     *
     * @param lineValues a list of all values to be written to a new line, separated through the CSV_SEPERATOR
     * @return if proceed or not
     *
     * add a new line to the file. between the values it's adding the CSV_SEPERATOR.
     */
    public boolean addLine (List<String> lineValues){

        if (accessMode == READ_ONLY_MODE) return false;
        if (lineValues.size()<=0) return false;

        StringBuffer sb = new StringBuffer();

        for (String s : lineValues){
            sb.append(s + CSV_SEPERATOR);
        }

        sb.deleteCharAt(sb.length()-1);
        this.readedContent.add(sb.toString());

        return true;
    }

    public boolean addLine (String fullLineValue){
        if (accessMode == READ_ONLY_MODE) return false;
        if (fullLineValue == null || fullLineValue.isEmpty()) return false;
        this.readedContent.add(fullLineValue);
        return true;
    }

    /**
     * @param lineValues a list of all values to be written to a new line, separated through the CSV_SEPERATOR
     * @param overwrite if true, it will be overwritten an already existing line, otherwise the line won't be added
     * @param uniquePosInLine the column (position) where to searching for the uniqueness String. first pos is 0.
     * @return if proceed or not
     *
     * add a new line with a unique value to the file. between the values it's appending the CSV_SEPERATOR.
     */
    public boolean addUniqueLine (List<String> lineValues, boolean overwrite, int uniquePosInLine){

        if (accessMode == READ_ONLY_MODE) return false;

        for (int i=0;i<getLines();i++){

            List<String> oneLine = getCsvValues(i);

            if (oneLine.get(uniquePosInLine).equals(lineValues.get(uniquePosInLine))){
                if (overwrite)	{
                    deleteLine(uniquePosInLine, oneLine.get(uniquePosInLine));
                    return addLine(lineValues);

                } else return false;
            }
        }

        return addLine(lineValues);
    }


    /**
     * @param valuePosInLine the column (position) where to searching for the String
     * @param valueToSearch the value to search
     * @return if the line was found and deleted
     */
    public boolean deleteLine (int valuePosInLine, String valueToSearch){

        if (accessMode == READ_ONLY_MODE) return false;

        for (int i=0;i<getLines();i++){
            List<String> oneLine = getCsvValues(i);
            if (oneLine.get(valuePosInLine).equals(valueToSearch)){
                this.readedContent.remove(i);
                return true;
            }
        }

        return false;
    }

    /**
     * In Write Mode it creates and writes the final file
     */
    public boolean writeAndCloseFile(){

        if (accessMode == READ_ONLY_MODE) return false;

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(this,true));

            for (String line : this.readedContent){
                writer.write(line);
                writer.newLine();
            }

            writer.close();
        }
        catch (Exception e){
            e.printStackTrace();
            return false;
        }

        FileTools.copyFile(this, new File(finalFileName), true);
        deleteOnExit();

        return true;

    }
}

