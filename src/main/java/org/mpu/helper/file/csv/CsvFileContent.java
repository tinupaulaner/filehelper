package org.mpu.helper.file.csv;

import java.util.*;

/**
 * @author Martin Pulfer
 * @version 1.00 (updated 29.03.2019)
 * @since 29.03.2019
 */
public abstract class CsvFileContent<T extends CsvFileEntry> {

    private String csvFileName;
    private int idCol;

    private boolean containsHeader;
    private CsvFileEntry header = null;

    private List<CsvFileEntry> plainFileEntries = new ArrayList<>();


    public CsvFileContent(String csvFileName, int idCol, boolean containsHeader){
        this.csvFileName = csvFileName;
        this.idCol = idCol;
        this.containsHeader = containsHeader;
        loadFileContent();
    }

    public CsvFileEntry getHeader(){
        return header;
    }

    private void loadFileContent(){

        try {

            CsvFile f = new CsvFile(csvFileName,CsvFile.READ_ONLY_MODE,false);
            List<CsvFileEntry> entries = new ArrayList<>();

            int lines = f.getLines();

            for (int i=0;i<lines;i++){
                CsvFileEntry cfe = new CsvFileEntry();
                cfe.setVals(f.getCsvValues(i));
                cfe.setVal(f.getFullLineValue(i));
                if (containsHeader && i == 0){
                    header = cfe;
                } else {
                    entries.add(cfe);
                }

            }

            plainFileEntries = entries;

        } catch (Exception ex){
            ex.printStackTrace();
        }

    }

    public boolean add (T ... entries){
        try {
            CsvFile f = new CsvFile(csvFileName, CsvFile.WRITE_MODE,false);

            for (T entry : entries){
                f.addLine(entry.getVals());
                plainFileEntries.add(entry);
            }

            f.writeAndCloseFile();
            return true;
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public boolean update (T ... entries){
        try {

            CsvFile f = new CsvFile(csvFileName, CsvFile.WRITE_MODE,true);
            Map<String, T> updateMap = new HashMap<>();

            for (T entry : entries){
                updateMap.put(entry.getVals().get(idCol),entry);
            }

            if (containsHeader){
                f.addLine(header.getVals());
            }

            for (CsvFileEntry cfe : plainFileEntries){

                String idVal = cfe.getVals().get(idCol);
                T result = updateMap.get(idVal);

                if (result != null){
                    f.addLine(result.getVals());
                } else {
                    f.addLine(cfe.getVals());
                }

            }

            f.writeAndCloseFile();
            loadFileContent();
            return true;

        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public boolean delete (String... ids){
        try {

            CsvFile f = new CsvFile(csvFileName, CsvFile.WRITE_MODE,true);
            List<String> toDelete = Arrays.asList(ids);

            if (containsHeader){
                f.addLine(header.getVals());
            }

            for (CsvFileEntry cfe : plainFileEntries){

                String idVal = cfe.getVals().get(idCol);

                if (!toDelete.contains(idVal)){
                    f.addLine(cfe.getVals());
                }

            }

            f.writeAndCloseFile();
            loadFileContent();
            return true;

        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    protected List<CsvFileEntry> getPlainEntries(){
        return plainFileEntries;
    }

    abstract public List<T> getEntries();

}
