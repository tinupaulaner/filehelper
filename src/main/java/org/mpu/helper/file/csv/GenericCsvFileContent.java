package org.mpu.helper.file.csv;

import java.util.List;

/**
 * @author Martin Pulfer
 * @version 1.00 (updated 29.03.2019)
 * @since 29.03.2019
 */
public class GenericCsvFileContent extends CsvFileContent<CsvFileEntry> {

    public GenericCsvFileContent(String csvFileName, int idCol, boolean containsHeader) {
        super(csvFileName, idCol, containsHeader);
    }

    @Override
    public List<CsvFileEntry> getEntries() {
        return getPlainEntries();
    }
}
