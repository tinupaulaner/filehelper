package org.mpu.helper.file;

import org.mpu.helper.file.csv.CsvFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Settings {

	public static String getSettingValue(String settingFile, String key){

		try {
			CsvFile file = new CsvFile(settingFile ,CsvFile.READ_ONLY_MODE,false);

			for (int i=0;i<file.getLines();i++){
				List<String> settings = file.getCsvValues(i);
				if (settings.get(0).equals(key)) return settings.get(1);
			}
			return null;			
		}

		catch (IOException e){
			return null;
		}
	}
	
	public static boolean setSettingValue (String settingFile, String key, String value){	
		if (key.isEmpty() || value.isEmpty()) return false;

		try {
			CsvFile file = new CsvFile(settingFile,CsvFile.WRITE_MODE,false);

			List<String> setting = new ArrayList<>();
			setting.add(key);
			setting.add(value);
			file.addUniqueLine(setting, true, 0);
			file.writeAndCloseFile();
			return true;			
		}

		catch (IOException e){
			return false;
		}
	}
	
}
