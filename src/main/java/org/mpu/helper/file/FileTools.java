package org.mpu.helper.file;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * File Tools
 * @author m.pulfer
 * @version 1.14
 * @since 03.04.2014
 *
 */

public abstract class FileTools {

	/**
	 * get all files (only files, no dirs) in a dir
	 * @param dir
	 * @param includeSubDir
	 * @return list of all files
	 */
	public static ArrayList<File> getFilesInDir(String dir, boolean includeSubDir){
		File path = new File (dir);
		File [] file = path.listFiles();

		ArrayList<File> list = new ArrayList<>();
		if (file == null) return list;

        for (File value : file) {
            if (value.isDirectory()) {
                if (includeSubDir) {
                    list.addAll(getFilesInDir(value.getAbsolutePath(), true));
                }
            } else list.add(value);
        }

		return list;

	}

	/**
	 * get all dirs in a dir
	 * @param dir
	 * @param includeSubDir
	 * @return list of all dirs
	 */
	public static ArrayList<File> getDirsInDir(String dir, boolean includeSubDir){

		File path = new File (dir);
		File [] file = path.listFiles();

		if (file == null) return null;

		ArrayList<File> list = new ArrayList<>();

        for (File value : file) {
            if (value.isDirectory()) {

                list.add(value);

                if (includeSubDir) {
                    list.addAll(getDirsInDir(value.getAbsolutePath(), true));
                }
            }
        }

		return list;

	}

	/**
	 * get the fileExtension of a fileName
	 * @param fileName
	 * @return the fileExtension ("txt","csv","exe" etc.) or null if filename does not contains a point = "."
	 */
	public static String getFileExtension (String fileName){
		return getFileExtension (new File(fileName));
	}

	/**
	 * get the fileExtension of a file
	 * @param file
	 * @return the fileExtension ("txt","csv","exe" etc.) or null if filename does not contains a point = "."
	 */
	public static String getFileExtension (File file){
		String fileName = file.getName();
		int pos = fileName.lastIndexOf(".");
		if (pos == -1) return null;
		return fileName.substring(pos+1);
	}

	/**
	 * renames a file
	 * @param file
	 * @param newNameWithoutExtension (eg. for rename a file called C:/textOld.txt to C:/textNew.txt choose "textNew")
	 * @return true, if file renamed
	 */
	public static boolean renameFile (File f, String newNameWithoutExtension){
		String ext = getFileExtension(f);
		String path = f.getParent() + "/";
		return f.renameTo(new File (path +  newNameWithoutExtension + "." + ext));
	}

	/**
	 * open a dialog to let the user choose a dir from the file system
	 * @param iniDir the dir where to start navigating. use null to open at last selected path
	 * @return the dir the user choosed or null if aborted or error
	 */
	public static File chooseDir(String iniDir){
		return chooseDir(iniDir,null);
	}

	/**
	 * open a dialog to let the user choose a dir from the file system
	 * @param iniDir the dir where to start navigating. use null to open at last selected path
	 * @param dialogTitle set a specific title for the dialog (null = default)
	 * @return the dir the user choosed or null if aborted or error
	 */
	public static File chooseDir(String iniDir, String dialogTitle){
		JFileChooser fc = new JFileChooser();
		if (iniDir!=null) fc.setCurrentDirectory(new File (iniDir));

		if (dialogTitle == null){
			fc.setDialogTitle("Choose Directory");
		} else fc.setDialogTitle(dialogTitle);

		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int result = fc.showOpenDialog(null);
		if (result == 0)return fc.getSelectedFile();
		return null;
	}

	/**
	 * open a dialog to let the user choose a file from the file system
	 * @param iniDir the dir where to start navigating. use null to open at last selected path
	 * @param fileExt set a file extension ("txt","exe" for example) to let the user choose a specific file
	 * @return the file the user choosed or null if aborted or error
	 */
	public static File chooseFile(String iniDir, String fileExt){
		return chooseFile(iniDir,fileExt,null);
	}

	/**
	 * open a dialog to let the user choose a file from the file system
	 * @param iniDir the dir where to start navigating. use null to open at last selected path
	 * @param fileExt set a file extension ("txt","exe" for example) to let the user choose a specific file
	 * @param dialogTitle set a specific title for the dialog (null = default)
	 * @return the file the user choosed or null if aborted or error
	 */
	public static File chooseFile(String iniDir, String fileExt, String dialogTitle){
		JFileChooser fc = new JFileChooser();
		if (iniDir!=null) fc.setCurrentDirectory(new File (iniDir));
		if (fileExt != null) fc.setFileFilter(new FileNameExtensionFilter("*." + fileExt,new String [] {fileExt}));

		if (dialogTitle == null){
			fc.setDialogTitle("Choose File");
		} else fc.setDialogTitle(dialogTitle);

		int result = fc.showOpenDialog(null);
		if (result == 0)return fc.getSelectedFile();
		return null;
	}

	/**
	 * open a dialog to let the user create a new file in the file system
	 * @param iniDir the dir where to start navigating. use null to open at last selected path
	 * @param fileExt set a file extension ("txt","exe" for example) to let the user save a specific file
	 * @param dialogTitle set a specific title for the dialog (null = default)
	 * @return the file created or null if aborted or error
	 */
	public static File chooseNewFileToSave(String iniDir, String fileExt, String dialogTitle){
		JFileChooser fc = new JFileChooser();
		if (iniDir!=null) fc.setCurrentDirectory(new File (iniDir));
		if (fileExt != null) fc.setFileFilter(new FileNameExtensionFilter("*." + fileExt,new String [] {fileExt}));

		if (dialogTitle == null){
			fc.setDialogTitle("Save File");
		} else fc.setDialogTitle(dialogTitle);

		fc.showSaveDialog(null);
		try {
			File f = fc.getSelectedFile();
			File f2 = new File(f.getAbsolutePath() + "." + fileExt);
			f2.createNewFile();
			return f2;
		}
		catch (Exception e){return null;}
	}

	/**
	 * copy a file
	 * @param sourceFile
	 * @param targetFile
	 * @param overwrite if true the target file will be overwritten if exists
	 * @return true, if targetFile was created
	 */
	public static boolean copyFile(File sourceFile, File targetFile, boolean overwrite){
		return copyFile(sourceFile.getAbsolutePath(), targetFile.getAbsolutePath(), overwrite);
	}

	/**
	 * copy a file
	 * @param sourceFileName
	 * @param targetFileName
	 * @param overwrite if true the target file will be overwritten if exists
	 * @return true, if targetFile was created
	 */
	public static boolean copyFile(String sourceFileName, String targetFileName, boolean overwrite){

		try{
			File f1 = new File (sourceFileName);
			File f2 = new File (targetFileName);

			if (!overwrite && f2.exists()) return false;

			InputStream in = new FileInputStream(f1);
			OutputStream out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			in.close();
			out.close();

			return true;

		}
		catch(Exception e){return false;}

	}


	/**
	 * copy a file from a uri to the temp user dir
	 * @param uri the source file location
	 * @return the copied file or null if error occurs
	 */
	public static File getFileFromInternet(String uri){
		try {

			URL url = new URL(uri);
			File f = File.createTempFile("temp", ".tmp");

			InputStream in = url.openStream();
			OutputStream out = Files.newOutputStream(f.toPath());

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}

			in.close();
			out.close();

			return f;

		} catch (Exception e){
			return null;
		}
	}


	/**
	 * copy a dir to another dir. if the target dir does not exist, it will be created
	 * @param sourceDir the dir name to copy
	 * @param targetDir the target dir name
	 * @param overwriteFiles if true, existing files will be overwritten
	 * @return true if all dirs were created and the files copied
	 */
	public static boolean copyDir (String sourceDir, String targetDir, boolean overwriteFiles){
		return copyDir(new File(sourceDir), new File(targetDir), overwriteFiles);
	}


	/**
	 * copy a dir to another dir. if the target dir does not exist, it will be created
	 * @param sourceDir the dir to copy
	 * @param targetDir the target dir
	 * @param overwriteFiles if true, existing files will be overwritten
	 * @return true if all dirs were created and the files copied
	 */
	public static boolean copyDir (File sourceDir, File targetDir, boolean overwriteFiles){
		if (sourceDir.isFile() || targetDir.isFile()){
			return false;
		}

		File [] data = sourceDir.listFiles();

		if (data == null){
			return false;
		}
		if (!targetDir.exists()) targetDir.mkdir();

		for (File f : data){
			if (f.isFile()){
				boolean result = copyFile(f.getAbsolutePath(), targetDir+"/"+f.getName(), overwriteFiles);

				if (!result){
					return false;
				}
			}
			else {

				File targetFile = new File(targetDir+"/"+f.getName());
				targetFile.mkdir();

				boolean result = copyDir (f, targetFile, overwriteFiles);
				if (!result){
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * copy a dir structure from a dir to the target dir
	 * @param fromDir the source dir to copy
	 * @param toDir the target where it should be copied
	 * @return true if all dirs could be created
	 */
	public static boolean createDirStructure (String fromDir, String toDir){

		List<File> dirs = getDirsInDir(fromDir, false);
		if (dirs == null) return false;

		for (File f : dirs){
			String path = toDir + "/" + f.getName();

			boolean cr1 = new File(path).mkdir();
			if (!cr1) return false;

			boolean cr2 = createDirStructure(f.getAbsolutePath(), path);
			if (!cr2) return false;
		}

		return true;

	}

	/**
	 * delete a dir completly
	 * @param dir the dir name to delete
	 * @return true if the dir could be deleted completly
	 */
	public static boolean deleteDir (String dir){
		return deleteDir(new File(dir));
	}

	/**
	 * delete a dir completly
	 * @param dir the dir to delete
	 * @return true if the dir could be deleted completly
	 */
	public static boolean deleteDir (File dir){
		if (dir.isFile() || !dir.exists()) return false;

		List<File> files = getFilesInDir(dir.getAbsolutePath(), false);

        for (File f : files){
			boolean result = f.delete();
			if (!result) return false;
		}

		List<File> dirs = getDirsInDir(dir.getAbsolutePath(), false);

		for (File f : dirs){
			boolean result = deleteDir (f);
			if (!result) return false;
		}

		return dir.delete();

	}

	/**
	 * copy a dir (dir, subdirs and all files) to another dir with the windows xcopy command. existing files will be overwritten
	 * @param fromDir the source dir name
	 * @param toDir the target dir name
	 * @return true if no error occured
	 */
	public static boolean copyDirsInWinBatch (String fromDir, String toDir){
		File f = new File(fromDir);

		if (!f.exists() || f.isFile() || new File(toDir).mkdirs()) return false;
		return executeWinXCopy(fromDir, toDir, new String []{"e","q","y"});
	}


	/**
	 * copy a file with the windows xcopy command
	 * @param fromFileName the file name to copy
	 * @param toFileName the target file name to create
	 * @param overwrite if true an existing target file will be overwritten
	 * @return true if no error occured
	 */
	public static boolean copyFileInWinBatch (String fromFileName, String toFileName, boolean overwrite){

		File f = new File(fromFileName);
		File target = new File(toFileName);

		if (!overwrite && target.exists()) return false;
		if (!f.exists() || f.isDirectory()) return false;

		String targetPath = target.getParentFile().getAbsolutePath() + "\\";

		return executeWinXCopy(fromFileName, targetPath, new String []{"y"});

	}

	public static File createZipFile (List<File> filesToZip, String targetFileName){

		byte[] buf = new byte[1024];
		File outputFile = new File (targetFileName);

		try {

			ZipOutputStream out = new ZipOutputStream(Files.newOutputStream(outputFile.toPath()));

			// Compress the files
			for (File f : filesToZip) {

				FileInputStream in = new FileInputStream(f);
				out.putNextEntry(new ZipEntry(f.getName()));

				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				out.closeEntry();
				in.close();
			}

			// Complete the ZIP file
			out.close();

		} catch (IOException e) {
		}

		return outputFile;

	}

	/**
	 * extract a zip File to a specific dir. if the dir does not exist it will be created
	 * @param zipFile the file to extract
	 * @param targetPath the target dir path
	 * @return true, if zip file could be extracted
	 */
	public static boolean extractFilesInZipFile (File zipFile, String targetPath){

		try {

			File targetDir = new File(targetPath);
			if (!targetDir.exists()) targetDir.mkdir();

			ZipFile zip = new ZipFile(zipFile);
			Enumeration<? extends ZipEntry> e = zip.entries();

			while (e.hasMoreElements()){
				ZipEntry ze = e.nextElement();

				File file = new File(targetPath + ze.getName());
				InputStream in = zip.getInputStream(ze);
				OutputStream out = Files.newOutputStream(file.toPath());

				byte[] buf = new byte[1024];
				int len;
				while ((len = in.read(buf)) > 0){
					out.write(buf, 0, len);
				}
				in.close();
				out.close();
			}

			return true;

		}

		catch (Exception e){return false;}

	}

	/**
	 * get the content of a file
	 * @param f the file
	 * @return a list of readed lines or if an error occurs the lines where could be readed
	 */
	public static List<String> getValuesInFile (File f){
		List<String> lines = new ArrayList<>();

		try {
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			while (br.ready()){
				lines.add(br.readLine());
			}

			br.close();
			fr.close();

		}

		catch (Exception e) {return lines;}
		return lines;
	}


	/**
	 * execute the windows xcopy command. the java process sleepes until the xcopy command is disposed.
	 * @param sourceName the source
	 * @param targetName the target
	 * @param params the official xcopy params. see help of xcopy (xcopy /?). use for example "H" or "Y" or "-Y"
	 * @return true if the job could be done.
	 */
	public static boolean executeWinXCopy(String sourceName, String targetName, String [] params){

		try {
			Runtime exec = Runtime.getRuntime();

			StringBuffer sb = new StringBuffer("xcopy.exe " + sourceName + " " + targetName + " ");
			for (String param : params){
				sb.append("/");
				sb.append(param);
			}

			Process p = exec.exec(sb.toString());

			while (p.getInputStream().read() != -1){}
			return p.exitValue()== 0;
		}

		catch (Exception e){
			return false;
		}
	}


	/**
	 * convert the content of a file to unicode characters
	 * @param fileName
	 * @return true if the file could be converted
	 */
	public static boolean convertToUnicode (String fileName){
		return convertToUnicode(new File(fileName));
	}


	/**
	 * convert the content of a file to unicode characters
	 * @param f the file to convert
	 * @return true if the file could be converted
	 */
	public static boolean convertToUnicode (File f){

		try {
			FileInputStream input = new FileInputStream(f);

			File tempFile = File.createTempFile("javaTempFile", "tmp");
			OutputStreamWriter output = new OutputStreamWriter(Files.newOutputStream(tempFile.toPath()),"unicode");

			while (true){
				int val = input.read();
				if (val == -1) break;
				output.write(val);
			}

			output.close();
			input.close();

			boolean result = copyFile(tempFile, f, true);
			tempFile.delete();

			return result;

		}

		catch (Exception e){return false;}
	}

	/**
	 * gets the file size in KB
	 * @param f the file
	 * @return size in KB
	 */
	public static Double getFileSizeInKB (File f){
		if (f == null) return null;
		Long len = f.length();
		if (len < 1024) return 0.0;
		return (len.doubleValue()/1024);
	}

	/**
	 * gets the file size in MB
	 * @param f the file
	 * @return size in MB
	 */
	public static Double getFileSizeInMB (File f){
		if (f == null) return null;
		Long len = f.length();
		if (len < 1024) return 0.0;
		return (len.doubleValue()/1024/1024);
	}

	/**
	 * @return the current user directory
	 */
	public static String getUserHomeDir (){
		return System.getProperty("user.home");
	}

	/**
	 * @return the current temp directory
	 */
	public static String getTempDir(){
		return System.getProperty("java.io.tmpdir");
	}

}
